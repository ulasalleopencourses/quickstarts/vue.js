# QuickStart Vue.js:
El objetivo de este proyecto es aprender sobre Vue.js.

# Herramientas:
* Node 12.14.0
* Vue.js 3.0
* Sublime text 3

# Código:
La carpeta código se encuentra el código desarrollado en los videos.

# Videos:

- **Video 1:** Vue.js, introducción e instalación 
    - Instalar Django
    - Aprender la estructura general de Django



- **Video 2:** Primeros pasos en Vue.js
    - Implementación de un menú.
    - Implementación de la parte gráfica.

- **Video 3:** Potencial de Vue.js
    - Conocer sobre la comunidad en Vue.js.
    - Conocer las mejores plataformas para aprender Vue.js.
    - Proyectos desarrollados en Vue.js.

# Creador:
    * Jhoel Huallpar Dorado
jhuallpard@ulasalle.edu.pe
